
all: test_hashtable bin/prueba bin/logdb

test_hashtable: src/test_hashtable.c
	gcc  -Iinclude/ src/test_hashtable.c -o bin/test_hashtable



lib/liblogdb.so:  obj/cerrar_db.o obj/abrir_db.o obj/conectar_db.o obj/crear_db.o obj/get_val.o obj/put_val.o obj/eliminar.o obj/csapp.o
	gcc -Wall -fPIC -shared -Iinclude/ $^ -lpthread -o lib/liblogdb.so 


obj/csapp.o: src/csapp.c include/csapp.h 
	gcc -Wall -c -fPIC src/csapp.c -I./include -o obj/csapp.o
obj/cerrar_db.o:src/cerrar_db.c
	gcc -Wall -c -fPIC $^ -I./include -o $@
obj/abrir_db.o: src/abrir_db.c
	gcc -Wall -c -fPIC $^ -I./include -o $@
obj/conectar_db.o: src/conectar_db.c
	gcc -Wall -c -fPIC $^ -I./include -o $@
obj/crear_db.o: src/crear_db.c
	gcc -Wall -c -fPIC $^ -I./include -o $@
obj/get_val.o: src/get_val.c
	gcc -Wall -c -fPIC $^ -I./include -o $@
obj/put_val.o: src/put_val.c
	gcc -Wall -c -fPIC $^ -I./include -o $@
obj/eliminar.o: src/eliminar.c
	gcc -Wall -c -fPIC $^ -I./include -o $@

obj/base.o: src/base.c
	gcc -Wall -c $^ -I./include -o $@
obj/indices.o: src/indices.c
	gcc -Wall -c $^ -I./include -o $@
obj/logdb.o: src/logdb.c
	gcc -Wall -c $^ -I./include -o $@
bin/logdb: obj/logdb.o obj/base.o obj/indices.o obj/csapp.o
	gcc -Wall obj/logdb.o obj/base.o obj/indices.o obj/csapp.o -o $@ -I./include -L./lib -lpthread


bin/prueba: src/prueba.c lib/liblogdb.so
	gcc -Wall  src/prueba.c -I./include lib/liblogdb.so -L./lib -o $@

.PHONY: clean
clean:
	rm bin/* obj/*
