#include "logdb.h"
#include "csapp.h"
int put_val(conexionlogdb *conexion, char *clave, char *valor){

	int n;
	if(conexion==NULL || clave == NULL || valor ==NULL)
		return -1;
	char solicitud[1000];
	strcpy(solicitud, "put ");
	strcat(solicitud, clave);
	strcat(solicitud, ":");
	strcat(solicitud, valor);
	strcat(solicitud, "\n");
	if(strcmp(clave,"@T!")==0 || strcmp(valor,"@T!")==0)
		return -1;
	
    
    if(rio_writen(conexion->sockfd, solicitud, strlen(solicitud))<0)
    	return -1;
    rio_t rio;
    rio_readinitb(&rio, conexion->sockfd);
    if(rio_readnb(&rio, &n, sizeof(int))<0)
    	return -1;
    return n;

}