#include "uthash.h"
#include <stdio.h>

typedef struct test{
	char* clave;
	char *valor;
	UT_hash_handle hh;		//necesario para poder usar la estrutura en la hashtable.
}test_struct;



int main(){

	test_struct *tabla = NULL;			//Necesario antes de usar la hashtable. Esta sera la referencia a la hashtable.
	

	test_struct *data = malloc(sizeof(test_struct)); //Usar punteros
	data->clave = "hola";
	data->valor = "dfdsfs";
	char *clave = data->clave;			//Importante: no mandar data->clave a HASH_ADD_STR, sino una nueva variable del tipo puntero.

	HASH_ADD_STR(tabla, clave, data);

	test_struct* data2  = malloc(sizeof(test_struct));
	data2->clave = "chao";
	data2->valor = "pepe";
	clave = data2->clave;

	HASH_ADD_STR(tabla, clave, data2);

	test_struct* data3  = malloc(sizeof(test_struct));
	data3->clave = "otra_clave";
	data3->valor = "un_valor_mas";
	clave = data3->clave;

	HASH_ADD_STR(tabla, clave, data3);

	//Buscar un elemento
	test_struct *encontrado = NULL;
	HASH_FIND_STR(tabla, "hola", encontrado);
	printf("valor encontrado: %s\n", encontrado->valor);

	HASH_FIND_STR(tabla, "chao", encontrado);
	printf("valor encontrado: %s\n", encontrado->valor);

	//Iterar sobre todo el contenido de la hash table:
	printf("Claves/valores guardados en la hashtable:\n");
	test_struct *elemento, *tmp;	//necesitamos tmp para iterar usando el macro

	//hh es el nombre del campo UT_hash_handle en la estructura.
	//En cada iteracion, elemento tendra un elemento de la hashtable.
  	HASH_ITER(hh, tabla, elemento, tmp) {
		printf("Clave: %s, Valo: %s\n", elemento->clave, elemento->valor);
	}
	

	//Reemplazar un elemento:
	test_struct* nuevo  = malloc(sizeof(test_struct));
	nuevo->clave = "hola";
	nuevo->valor = "otro_valor";
	test_struct* viejo  = NULL;
	clave = nuevo->clave;
	HASH_REPLACE_STR(tabla, clave, nuevo, viejo);		//devuelve el objeto anterior. A este le hacemos free() si ya no lo necesitamos.

	test_struct *nuevo_encontrado = NULL;
	HASH_FIND_STR(tabla, "hola", nuevo_encontrado);
	printf("Valor viejo: %s, Valor nuevo: %s\n", viejo->valor, nuevo_encontrado->valor);
	free(viejo);

	//Para eliminar, mandamos puntero a estructura a eliminar (podemos usar HASH_FIND para encontrar este puntero
	//Eliminar saca la estructura de la hashtable, pero no libera la memoria. Eso
	//Lo tenemos que hacer nosotros.
	test_struct *a_remover = NULL;
	HASH_FIND_STR(tabla, "chao", a_remover);
	HASH_DEL(tabla, a_remover);	//remover de la hashtable
	free(a_remover);
	HASH_FIND_STR(tabla, "chao", a_remover);

	if(a_remover == NULL){
		printf("No se encontro el elemento con clave %s\n", "chao");
	}
	
	return 0;
}
