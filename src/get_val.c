#include "logdb.h"
#include "csapp.h"


char *get_val(conexionlogdb *conexion, char *clave){

	char c[500];
	if(conexion==NULL)
		return NULL;
	char solicitud[500]="get ";
	strcat(solicitud, clave);
	strcat(solicitud, "\n");
	if(strcmp(clave,"@T!")==0){
        return NULL;
    }

    if(rio_writen(conexion->sockfd, solicitud, strlen(solicitud))<0)
    	return NULL;
    rio_t rio;
    rio_readinitb(&rio, conexion->sockfd);
    int r = 0;
    if((r=rio_readlineb(&rio, c, 500))<0)
    	return NULL;
     c[r-1] = '\0';
    if(strcmp(c,"@T!")==0){
        return NULL;
    }

    return strdup(c);

}