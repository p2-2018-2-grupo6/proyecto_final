#include "base.h"
#include "indice.h"

char *dir_bases;
char *dir_indices;
struct obj_hashtable *directorios;


int initserver(int type, const struct sockaddr *addr, socklen_t alen, int qlen){

	int fd;
	int err = 0;
	
	if((fd = socket(addr->sa_family, type, 0)) < 0)
		return -1;

	if(bind(fd, addr, alen) < 0)
		goto errout;

	if(type == SOCK_STREAM || type == SOCK_SEQPACKET){
		
		if(listen(fd, qlen) < 0)
			goto errout;
	}
	return fd;
	errout:
	err = errno;
	close(fd);
	errno = err;
	return (-1);
}


int main(int argc, char **argv){
	if(argc!=4){
		printf("No hay suficientes argumentos\n");
		return -1;
	}
	char string[2000];
	dir_bases = string;
	strcpy(dir_bases,argv[1]);
	strcat(dir_bases,"/bases/");
	umask(0);
	directorios = cargar_directorio();
	char dir_indices1[2000];
	dir_indices = dir_indices1;
	strcpy(dir_indices, argv[1]);
	strcat(dir_indices, "/indices/");
	mkdir(dir_indices, 0777);

	mkdir(dir_bases, 0777);
	
	int puerto = atoi(argv[3]);
	char *ip = argv[2];
	 //Direccion del servidor
	struct sockaddr_in direccion_servidor;

	memset(&direccion_servidor, 0, sizeof(direccion_servidor));	//ponemos en 0 la estructura direccion_servidor

	//llenamos los campos
	direccion_servidor.sin_family = AF_INET;		//IPv4
	direccion_servidor.sin_port = htons(puerto);		//Convertimos el numero de puerto al endianness de la red
	direccion_servidor.sin_addr.s_addr = inet_addr(ip) ;	//Nos vinculamos a la interface localhost o podemos usar INADDR_ANY para ligarnos A TODAS las interfaces

	
	int sockfd;
	//inicalizamos servidor (AF_INET + SOCK_STREAM = TCP)
	if( (sockfd = initserver(SOCK_STREAM, (struct sockaddr *)&direccion_servidor, sizeof(direccion_servidor), 1000)) < 0){	//Hasta 1000 solicitudes en cola 
		printf("Error al inicializar el servidor\n");
		return -1;	
	}		
	
	int n;
	BASE *base = NULL;
	char recibido[2000];
	char respuesta[2000];
	while (1) {
		
		int cliente = accept(sockfd, NULL, NULL);
		rio_t rio;
		rio_readinitb(&rio, cliente);
		
		while((n = rio_readlineb(&rio, recibido, 2000)) > 0) {
			
			recibido[n-1] = '\0';
			char *mensaje = strchr(recibido,' ');
			if(mensaje!=NULL){
				*mensaje='\0';
				mensaje+=1;
			}

			if(strncmp(recibido,"crear",strlen("crear "))==0){
				int r = -1;
				char nombre[2000];
				strcpy(nombre,dir_bases);
				strcat(nombre,mensaje);

				struct obj_hashtable *tmp = NULL;


				HASH_FIND_STR(directorios,mensaje,tmp);
				if(tmp == NULL){
					r = crear_base(nombre);
					fflush(stdout);
					if(!r){
						agregar_directorio(mensaje,nombre);
					}
					
				}
				
				rio_writen(cliente, &r,sizeof(int));
			}else if(strncmp(recibido,"abrir",strlen("abrir "))==0){
				int r = -1;
				char tmp[2000];
				strcpy(tmp, dir_bases);
				strcat(tmp,mensaje);
				
				char tmp1[2000];
				strcpy(tmp1, dir_indices);

				strcat(tmp1,mensaje);
				base = abrir_base(tmp,tmp1);
				if(base!=NULL){
					r=0;
				}
					
				rio_writen(cliente, &r,sizeof(int));
				
				
			}else if(strncmp(recibido,"get",strlen("get "))==0){
				if(base==NULL){
					strcpy(respuesta,"@T!");
					strcat(respuesta,"\n");
				}else{

					char *r_get = get_base(base, mensaje);
					if(r_get==NULL){
						strcpy(respuesta,"@T!");
						strcat(respuesta,"\n");
					}else{
						strcpy(respuesta,r_get);
						strcat(respuesta, "\n");	
						free(r_get);
					}
				}


				rio_writen(cliente,respuesta,strlen(respuesta));

			}else if(strncmp(recibido,"put",strlen("put "))==0){
				int r = -1;
				if(base==NULL){
					rio_writen(cliente, &r,sizeof(int));
				}else{
					char *valor = strchr(mensaje,':');
					*valor='\0';
					r=put_base(base,mensaje,valor+1);
				
					rio_writen(cliente,&r,sizeof(int));
				}
				
			}else if(strncmp(recibido,"eliminar",strlen("eliminar "))==0){
				int r = -1;
				if(base!=NULL){			
					r = put_base(base, mensaje, "@T!");
				}
				rio_writen(cliente,&r,sizeof(int));

			}else if(strncmp(recibido,"cerrar",strlen("cerrar "))==0){
				if(base!=NULL){
					
					guardar_archivo_indice(base->ht,base->indice);

					fclose(base->log);
					HASH_CLEAR(hh,base->ht);
					free(base->nombre);
					free(base->indice);
					free(base);
					
					base=NULL;
				}
			}


		}
		if(base!=NULL){
			guardar_archivo_indice(base->ht,base->indice);
			fclose(base->log);
			HASH_CLEAR(hh,base->ht);
			free(base->nombre);
			free(base->indice);
			free(base);
		}
		close(cliente);
	}
	exit(0);
}