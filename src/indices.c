#include "indice.h"

void guardar_indice(struct obj_hashtable **hashtable, char *clave, FILE *f){
	
	if(fseek(f, 0, SEEK_END)!=0 || clave==NULL){
		return;
	}

	struct obj_hashtable *tmp = NULL;
	HASH_FIND_STR(*hashtable, clave,tmp);
	
	int ind=ftell(f);
	if(tmp!=NULL){
		*((int *)tmp->valor)=ind;
	}else{
		struct obj_hashtable *tmp2 = malloc(sizeof(struct obj_hashtable));
		tmp2->clave=strdup(clave);
		tmp2->valor = malloc(sizeof(int));
		*((int *)tmp2->valor) = ind;
		HASH_ADD_STR(*hashtable,clave,tmp2);
	}

	
	
}

char *buscar_indice(struct obj_hashtable *hashtable, char* clave, FILE *f){
	struct obj_hashtable *tmp = NULL;
	HASH_FIND_STR(hashtable,clave, tmp);
	
	if(tmp==NULL){
		return NULL;
	}
	int pos=fseek(f, *((int *)tmp->valor), SEEK_SET);
	if(pos==0){

		int len;
		char linea[4000];
		fgets(linea, 4000, f);
		len=strlen(linea);
		linea[len-1] = '\0';
		char *separador=strchr(linea, ':');
		separador++;
		fseek(f, 0, SEEK_END);
		return strdup(separador);
	}
	fseek(f, 0, SEEK_END);
	return NULL;
}

void guardar_archivo_indice(struct obj_hashtable *hashtable, char *ruta){
	FILE *fp=fopen(ruta, "w");
	char linea[4000];
	struct obj_hashtable *i, *i2;
	HASH_ITER(hh,hashtable, i, i2) {
		sprintf(linea,"%s:%d\n",i->clave,*((int *)i->valor));
		fputs(linea, fp);
	}
	fclose(fp);
}

struct obj_hashtable *cargar_hashtable(char *ruta){
	struct obj_hashtable *ht=NULL;
	FILE *fp=fopen(ruta, "r");
	if(fp==NULL){
		return ht;
	}
	char linea[4000];	
	while(fgets(linea, 4000, fp)!=NULL){
		char *separador=strchr(linea, ':');
		*separador='\0';
		int *i = (int * ) malloc(sizeof(int));
		*i=atoi(separador+1);
		struct obj_hashtable *tmp = malloc(sizeof(struct obj_hashtable));
		tmp->clave = strdup(linea);
		tmp->valor = i;
		HASH_ADD_STR(ht, clave, tmp);
	}
	return ht;
}

