#include "logdb.h"
#include "csapp.h"
int crear_db(conexionlogdb *conexion, char *nombre_db){
	
	int n;
	if(conexion==NULL)
		return -1;
	char solicitud[500]="crear ";
	strcat(solicitud, nombre_db);
	strcat(solicitud, "\n");


    if(rio_writen(conexion->sockfd, solicitud, strlen(solicitud))<0) 
    	return -1;
    rio_t rio;
    rio_readinitb(&rio, conexion->sockfd);

    if(rio_readnb(&rio, &n, sizeof(int))<0)
    	return -1;

    conexion->nombredb=strdup(nombre_db);
    return n;

}