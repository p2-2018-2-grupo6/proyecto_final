#include "logdb.h"
#include "csapp.h"
void cerrar_db(conexionlogdb *conexion){

	if(conexion==NULL)
		return;
	char solicitud[500]="cerrar ";
	strcat(solicitud, conexion->nombredb);
	strcat(solicitud, "\n");	
    rio_writen(conexion->sockfd, solicitud, strlen(solicitud));

	close(conexion->sockfd);
	free(conexion->ip);
	free(conexion->nombredb);
	
	free(conexion);

}