#include "logdb.h"
#include "csapp.h"
int eliminar(conexionlogdb *conexion, char *clave){

	int n;
	if(conexion==NULL)
		return -1;
	char solicitud[500]="eliminar ";
	strcat(solicitud, clave);
	strcat(solicitud, "\n");
	

    if(rio_writen(conexion->sockfd, solicitud, strlen(solicitud))<0)
    	return -1;
    rio_t rio;
    rio_readinitb(&rio, conexion->sockfd); 
    if(rio_readnb(&rio, &n, sizeof(int))<0)
    	return -1;
    return n;

}