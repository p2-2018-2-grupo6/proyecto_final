#include "logdb.h"

int main(int argc, char **argv){
	conexionlogdb *con = conectar_db("127.0.0.1",64121);

	char *respuesta = NULL;
	printf("%d\n",crear_db(con,"db_prueba"));

	printf("%d\n",abrir_db(con,"db_prueba"));
	printf("%d\n",put_val(con, "clave", "valor"));	
	printf("%d\n",put_val(con, "clave1", "valor1"));	
	printf("%d\n",put_val(con, "clave2", "valor2"));	
	printf("%d\n",put_val(con, "clave4", "valor3"));	
	printf("%d\n",put_val(con, "clave3", "valor4"));	

	printf("%d\n",eliminar(con,"clave"));	
	printf("%d\n",eliminar(con,"clave3"));	
	respuesta = get_val(con, "clave");
	printf("%s\n", (respuesta==NULL)?"null":respuesta);
	respuesta = get_val(con, "no existe");
	printf("%s\n", (respuesta==NULL)?"null":respuesta);
	respuesta = get_val(con, "clave2");
	printf("%s\n", (respuesta==NULL)?"null":respuesta);
	respuesta = get_val(con, "clave4");
	printf("%s\n", (respuesta==NULL)?"null":respuesta);

	printf("%d\n",put_val(con, "clave44", "valor44"));	
	printf("%d\n",put_val(con, "clave22", "valor55"));	
	printf("%d\n",put_val(con, "clave33", "valor66"));
	printf("%s\n",get_val(con, "clave44"));	
	printf("%s\n",get_val(con, "clave22"));	
	printf("%s\n",get_val(con, "clave33"));	

	cerrar_db(con);
	
	
	return 0;
}