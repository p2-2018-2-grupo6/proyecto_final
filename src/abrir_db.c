#include "logdb.h"
#include "csapp.h"
int abrir_db(conexionlogdb *conexion, char *nombre_db){

	int n = -1;
	if(conexion==NULL)
		return -1;
	char solicitud[500]="abrir ";
	strcat(solicitud, nombre_db);
	strcat(solicitud, "\n");
	
    
    if(rio_writen(conexion->sockfd, solicitud, strlen(solicitud))<0)
    	return -1;
    rio_t rio;
    rio_readinitb(&rio, conexion->sockfd);
    if(rio_readnb(&rio, &n, sizeof(int))<0)
    	return -1;
    return n;

}