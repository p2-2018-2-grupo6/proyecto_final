#include "base.h"
#include "indice.h"



int crear_base(char *nombre_db){

	FILE *fp = fopen(nombre_db, "r");
	if(fp!=NULL){
		
		return -1;
	}

	fp = fopen(nombre_db,"w");

	fclose(fp);
	return 0;
}

BASE *abrir_base(char *nombre_db, char *indice_db){
	
	
	
	FILE *fp=fopen(nombre_db, "a+");
	if(fp==NULL){
		return NULL;
	}
	BASE *base=(BASE *)malloc(sizeof(BASE));
	struct obj_hashtable *ht=cargar_hashtable(indice_db);
	base->nombre=strdup(nombre_db);
	base->indice= strdup(indice_db);
	base->ht=ht;
	base->log=fp;
	
    
	return base;
}

int put_base(BASE *base, char *clave, char *valor){
	if(base==NULL)
		return 0;
	char linea[1000];
	sprintf(linea, "%s:%s\n", clave, valor);
	
	guardar_indice(&(base->ht), clave, base->log);
	if(fputs(linea, base->log)>0)
		return 0;
	
	
	return -1;
}

char *get_base(BASE *base, char *clave){
	if(base==NULL)
		return NULL;

	char *temp;
	
	temp = buscar_indice(base->ht, clave, base->log);
	
	return temp;

}


int cerrar_conexion(BASE *base){
	if(base==NULL){
		return -1;
	}

		
	

	return 0;
}

void agregar_directorio(char *nombre, char* directorio){
	FILE *f = fopen("bases.txt","a");
	char linea[MAXLINE];
	if(f!=NULL){
		sprintf(linea,"%s:%s\n",nombre,directorio);
		fputs(linea,f);
		fclose(f);
	}
}

struct obj_hashtable *cargar_directorio(){
	struct obj_hashtable *ht = NULL;
	char linea[2000];
	FILE *f = fopen("bases_archivo","r");
	if(f!=NULL){
		while(fgets(linea, 2000,f)!=NULL){
			char *puntos = strchr(linea,':');
			printf("%s\n", linea);
			int dif = (puntos-linea);
			int n = strlen(linea);
			linea[dif] = '\0';
			linea[n-1] = '\0';
			char *dir = strdup(linea+dif+1);
			struct obj_hashtable *tmp = malloc(sizeof(struct obj_hashtable));
			tmp->clave = strdup(linea);
			tmp->valor = dir;
			HASH_ADD_STR(ht,clave,tmp);


		}
	}	
	return ht;
}
