#ifndef BASE_H
#define BASE_H

#include "hashtable.h"
#include "csapp.h"



typedef struct{
    char *nombre;
    struct obj_hashtable *ht;
    char *indice;
    FILE *log;

} BASE;


int crear_base(char *nombre_base);

BASE *abrir_base(char *nombre_base, char *indice);

int put_base(BASE *base, char *clave, char *valor);

char *get_base(BASE *base, char *clave);

void agregar_directorio(char *nombre, char* directorio);

struct obj_hashtable *cargar_directorio();

#endif