#ifndef INDICE_H
#define INDICE_H

#include "hashtable.h"
#include <stdio.h>

void guardar_indice(struct obj_hashtable **tabla, char *clave, FILE *f);

char *buscar_indice(struct obj_hashtable *tabla, char* clave, FILE *f);

void guardar_archivo_indice(struct obj_hashtable *tabla, char *ruta);

struct obj_hashtable *cargar_hashtable(char *ruta);
#endif